package com.qa.automation.daraz.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.qa.automation.daraz.base.Testbase;

public class landingPage extends Testbase {
	
	public landingPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "Level_1_Category_No2")
	WebElement Electronics;
	
	@FindBy(xpath = "//*[@id=\"J_8018372580\"]/div/ul/ul[2]/li[6]")
	WebElement ComputerPeriphels;
	
	@FindBy(xpath = "//*[@id=\"J_8018372580\"]/div/ul/ul[2]/li[6]/ul/li[2]")
	WebElement Keyword;
	
	public String getkeywordpagetitle() {
		Actions actions = new Actions(driver); 
		actions.moveToElement(Electronics).perform();
		actions.moveToElement(ComputerPeriphels).perform();
		Keyword.click();
		String title = driver.getTitle();
		return title;
	}

}
