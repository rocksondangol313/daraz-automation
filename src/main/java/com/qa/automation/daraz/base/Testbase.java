package com.qa.automation.daraz.base;

import java.io.FileInputStream;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Testbase {
	
	public static WebDriver driver;
	public static Properties prop;
	
	public Testbase() {
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream("C:\\Users\\dell\\eclipse-workspace\\daraz\\src\\main\\java\\com\\qa\\automation\\daraz\\config\\config.properties");
			prop.load(ip);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void initialization() {
		String browsername = prop.getProperty("browser");
		if(browsername.equals("firebox")) {
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\dell\\eclipse-workspace\\daraz\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
		if(browsername.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\dell\\eclipse-workspace\\daraz\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get("https://www.daraz.com.np/#");
	}

}
