package com.qa.automation.daraz;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qa.automation.daraz.base.Testbase;
import com.qa.automation.daraz.pages.landingPage;

public class landingpageTest extends Testbase {
	
	landingPage lp;
	
	public landingpageTest() {
		super();
	}
	
	@BeforeMethod
	public void setup() {
		initialization();
		lp = new landingPage();
	}
	
	@Test
	public void test() {
		String Actualtitle = lp.getkeywordpagetitle();
		String Expectedtitle = "Keyboard Price in Nepal - Buy Computer Keyboard Online - Daraz.com.np";
		assertEquals(Actualtitle, Expectedtitle);
	}
	
	@AfterMethod
	public void tearDown() {
		driver.close();
	}
}
